### Rolling ship

A basic mobile game where you collect balls and avoid obstacles.

Swipe finger left & right to move the ship.

[High resolution demo](https://gfycat.com/hollowmildcaimanlizard)

![Demo](https://thumbs.gfycat.com/HollowMildCaimanlizard-size_restricted.gif)
