﻿using UnityEngine;

public class NormalsDebug : MonoBehaviour
{
    // Update is called once per frame
    private void Update()
    {
        foreach (Transform child in transform)
        {
            Debug.DrawLine(child.position, child.position + child.GetComponent<MeshFilter>().mesh.normals[0], Color.red);
        }
    }
}
