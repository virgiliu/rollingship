﻿using UnityEngine;

/// <summary>
/// Used in conjunction with ObjectPoolManager.
/// Use instead of instantiating objects at runtime, it can cause frame drops.
/// </summary>
[System.Serializable]
public class ObjectPool
{
    /// <summary>
    /// Pool name
    /// </summary>
    public string name;

    /// <summary>
    /// What object to instantiate in the pool
    /// </summary>
    public GameObject prefab;

    /// <summary>
    /// How many objects to instantiate in the pool
    /// </summary>
    public int size;
}
