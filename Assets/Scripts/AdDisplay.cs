﻿using UnityEngine;

public class AdDisplay : MonoBehaviour
{
    [SerializeField] GameObject adImage;

    private void OnEnable()
    {
        GameController.OnGameStart += GameController_OnGameStart;
        GameController.OnGameEnd += GameController_OnGameEnd;
    }

    private void OnDisable()
    {
        GameController.OnGameStart -= GameController_OnGameStart;
        GameController.OnGameEnd -= GameController_OnGameEnd;
    }

    private void GameController_OnGameEnd()
    {
        //  50% chance of showing an ad
        if (Random.Range(0f, 1f) >= 0.5f)
        {
            adImage.SetActive(true);
        }
    }

    private void GameController_OnGameStart()
    {
        adImage.SetActive(false);
    }
}
