﻿public static class Names
{
    public static readonly string ObstacleTag = "Obstacle";
    public static readonly string ScoreZoneTag = "ScoreZone";
    public static readonly string ObstacleParentTag = "ObstacleParent";
    public static readonly string PlayerTag = "Player";
    public static readonly string PickupTag = "Pickup";
    public static readonly string ScoreSound = "Score";
    public static readonly string CollisionSound = "Collision";
    public static readonly string EngineSound = "Engine";
}