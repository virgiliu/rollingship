﻿using UnityEngine;

public class ObstacleAnimationController : MonoBehaviour
{
    [SerializeField] private Animator anim;

    const string despawnTrigger = "Despawn";

    private void OnEnable()
    {
        GameController.OnGameEnd += GameController_OnGameEnd;
    }

    private void OnDisable()
    {
        GameController.OnGameEnd -= GameController_OnGameEnd;
    }

    private void GameController_OnGameEnd()
    {
        anim.ResetTrigger(despawnTrigger);
        anim.SetTrigger(despawnTrigger);
    }
}

