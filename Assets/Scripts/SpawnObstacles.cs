﻿using System.Collections.Generic;
using UnityEngine;


public class SpawnObstacles : MonoBehaviour
{
    /// <summary>
    /// How often to spawn objects, in seconds
    /// </summary>
    [SerializeField] private float spawnFrequency = 1.2f;


    /// <summary>
    /// The direction and speed of the obstacles.
    /// They will move at a constant velocity.
    /// </summary>
    [SerializeField] private Vector3 velocity = -Vector3.forward;

    [SerializeField] private GameObject obstaclePrefab;

    [SerializeField] private GameObject obstacleParentPrefab;

    // Use to adjust spawn frequency during runtime instead of using InvokeRepeating
    private float lastSpawnTime = 0;

    private const string obstacleParentName = "ObstacleRing";

    /// <summary>
    /// How many edges are on the obstacle ring
    /// </summary>
    private const int circumferenceEdges = 32;


    // The angle you need to use when spawning & rotating obstacles,
    // so that they fit seamlessly on a circle.
    // To get to this number use the following formula:
    // 360 / (CircumferenceEdges / SegmentEdges)
    //
    // For a 32 edge circle, with a 1 segment edge per obstacle:
    // 360 / (32 / 1) = 11.25
    //
    // Further "reading": https://youtu.be/skO7bpdmDuE?t=206
    private const float obstacleRotationAngle = 11.25f;
    private const int minObstacleCount = 3;

    // Instead of computing a random divisor of 32 (circumferenceEdges) at runtime, get a random divisor from this list
    // Skip 1 as a divisor because 1unit gaps are not fun
    private readonly int[] divisorsOf32 = { 2, 4, 8, 16 };

    private bool canSpawn = false;

    private List<GameObject> spawnedObstacles = new List<GameObject>();

    private void OnEnable()
    {
        GameController.OnGameStart += GameController_OnGameStart;
        GameController.OnGameEnd += GameController_OnGameEnd;
    }

    private void OnDisable()
    {
        GameController.OnGameStart -= GameController_OnGameStart;
        GameController.OnGameEnd -= GameController_OnGameEnd;
    }

    private void GameController_OnGameStart()
    {
        canSpawn = true;
        // Despawn obstacles on game start, instead of game end, 
        // so that the player can see the obstacles retracting 
        // when the game ends instead of them just disappearing
        DespawnObstacles();
    }

    private void GameController_OnGameEnd()
    {
        canSpawn = false;
    }

    private void Update()
    {
        if (canSpawn && Time.time - lastSpawnTime > spawnFrequency)
        {
            Spawn();
            lastSpawnTime = Time.time;
        }
    }

    private void Spawn()
    {
        GameObject obstacleParent = CreateObstacleRing();
        spawnedObstacles.Add(obstacleParent);
    }

    private void DespawnObstacles()
    {
        foreach(var obstacle in spawnedObstacles)
        {
            Destroy(obstacle); // TODO: Return obstacle to object pool
        }

        spawnedObstacles.Clear();
    }


    public GameObject CreateObstacleRing()
    {
        GameObject parent = Instantiate(obstacleParentPrefab); // TODO: Use object pool
        parent.name = obstacleParentName;

        parent.GetComponent<ConstantMove>().velocity = velocity;

        parent.transform.position = transform.position;

        float rand = Random.Range(0.0f, 1.0f);
        // 50% chance of using mirror obstacles, otherwise use spaced obstacles
        bool useMirrorObstacles = rand == 0;

        if (useMirrorObstacles)
        {
            CreateMirroredObstacles(parent);
        }
        else
        {
            CreateSpacedObstacles(parent);
        }

        return parent;
    }

    private void CreateSpacedObstacles(GameObject parent)
    {
        // Get a random obstacle count, that is a divisor of circumference edges (32)
        // This is so that the obstacles wrap nicely around a circle
        int obstacleCount = divisorsOf32[Random.Range(0, divisorsOf32.Length)];

        float initialAngle = Random.Range(0, 360);

        float spawnAngle = initialAngle;

        for (int i = 0; i < circumferenceEdges; i++)
        {
            bool shouldAddObstacle = ShouldAddObstcle(obstacleCount, i);
            if (shouldAddObstacle)
            {
                SpawnObstacleInParent(parent, spawnAngle);
            }
            spawnAngle += obstacleRotationAngle;
        }
    }


    /// <summary>
    /// We want to generate obstacles in a loopable pattern, like 1100110011001100 or 111100001111000011110000 etc.
    /// This method will tell us if we should create an object or not, based on the number of obstacles and the current iteration
    /// </summary>
    /// <param name="obstacleCount"></param>
    /// <param name="i"></param>
    /// <returns></returns>
    private bool ShouldAddObstcle(int obstacleCount, int i)
    {
        return i == 0 || (i / obstacleCount) % 2 == 0;
    }

    private void CreateMirroredObstacles(GameObject parent)
    {
        int obstacleCount = Random.Range(minObstacleCount, circumferenceEdges / 2);

        float initialAngle = Random.Range(0, 360);

        for (int i = 0; i < obstacleCount; i++)
        {
            float spawnAngle = initialAngle + i * obstacleRotationAngle;
            SpawnObstacleInParent(parent, spawnAngle);

            // Spawn mirrored as well
            float spawnAngleMirrored = 180 + initialAngle + i * obstacleRotationAngle;
            SpawnObstacleInParent(parent, spawnAngleMirrored);
        }
    }

    private void SpawnObstacleInParent(GameObject parent, float spawnAngle)
    {
        GameObject obstacle = Instantiate(obstaclePrefab, parent.transform);
        obstacle.transform.localRotation = Quaternion.Euler(0, 0, spawnAngle);
    }
}
