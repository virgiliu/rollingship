﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Use this class instead of instantiating obstacles or other things at runtime.
/// Will improve runtime performance at the cost of memory usage.
/// Should be fine with high pool sizes on most modern devices. Should.
/// </summary>
public class ObjectPoolManager : MonoBehaviour
{
    public static ObjectPoolManager instance;

    [SerializeField] List<ObjectPool> poolsToCreate;

    Dictionary<string, Queue<GameObject>> pools;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            if (transform.parent == null)
            {
                DontDestroyOnLoad(gameObject);
            }
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        foreach (ObjectPool pool in poolsToCreate)
        {
            var newPool = new Queue<GameObject>();

            for (int poolObjCount = 0; poolObjCount < pool.size; poolObjCount++)
            {
                GameObject spawnedObj = Instantiate(pool.prefab);
                spawnedObj.SetActive(false);
                newPool.Enqueue(spawnedObj);
            }
        }
    }

    private void ResetObject(GameObject spawnedObj)
    {
        spawnedObj.transform.SetParent(null);
        spawnedObj.transform.position = Vector3.zero;
        spawnedObj.transform.rotation = Quaternion.identity;
    }

    public GameObject SpawnFromPool(string poolTag)
    {
        if(pools.ContainsKey(poolTag))
        {
            // Get the first object in the pool
            GameObject obj = pools[tag].Dequeue();

            // Make sure the object doesn't have any position or rotation applied from previous uses
            ResetObject(obj);

            // Add the object back to the pool so it can be reused
            pools[tag].Enqueue(obj);

            return obj;
        }
        else
        {
            Debug.LogWarning(string.Format("Pool with tag '{0}' doesn't exist", poolTag));
            return null;
        }
    }

    // Use instead of Destroy for pooled objects
    public void Dispose(GameObject obj)
    {
        obj.SetActive(false);
        obj.transform.parent = null;

        // TODO: Protect pool objects from being destroyed if their parent is destroyed
    }

}