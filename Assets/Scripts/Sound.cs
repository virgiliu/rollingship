﻿using System;
using UnityEngine;

[System.Serializable]
public class Sound
{
    public string name;

    public AudioClip clip;

    [Range(0f, 1f)]
    public float volume;

    public bool playOnAwake = false;

    public bool loop;


    [HideInInspector]
    public AudioSource source;

    internal void InitSource()
    {
        source.clip = clip;
        source.playOnAwake = playOnAwake;
        source.volume = volume;
        source.loop = loop;

        if(playOnAwake)
        {
            source.Play();
        }
    }
}