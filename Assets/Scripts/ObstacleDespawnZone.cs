﻿using UnityEngine;

public class ObstacleDespawnZone : MonoBehaviour
{
    public delegate void ObstacleDespawnAction(GameObject obstacle);
    public static event ObstacleDespawnAction OnObstacleShouldDespawn;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == Names.ObstacleParentTag)
        {
            Destroy(other.gameObject);
        }
    }
}
