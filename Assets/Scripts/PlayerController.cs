﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float rotateSpeed = 200f;
    private bool canMove = false;
    private Touch touch;

    // How much a finger should move during a frame before reacting to the movement
    // Value determined empirically
    private float moveThreshold = 0.5f;

    [SerializeField] GameObject playerModel;

    private void OnEnable()
    {
        GameController.OnGameStart += GameController_OnGameStart;
        GameController.OnGameEnd += GameController_OnGameEnd;
    }

    private void GameController_OnGameStart()
    {
        canMove = true;
        playerModel.SetActive(true);
    }

    private void GameController_OnGameEnd()
    {
        canMove = false;
        playerModel.SetActive(false);
    }

    private void Update()
    {
        if (canMove && Input.touchCount > 0)
        {
            // We care only about first finger to touch the screen
            touch = Input.GetTouch(0);

            float deltaX = touch.deltaPosition.x;

            if (Mathf.Abs(deltaX) > moveThreshold)
            {
                transform.Rotate(transform.forward, deltaX * rotateSpeed * Time.deltaTime);
            }
        }
#if UNITY_EDITOR
        if (canMove)
        {
            transform.Rotate(transform.forward, Input.GetAxis("Horizontal") * rotateSpeed * Time.deltaTime);
        }
#endif

    }
}
