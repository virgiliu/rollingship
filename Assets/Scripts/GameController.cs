﻿using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController instance = null;

    public delegate void EndGameAction();
    public static event EndGameAction OnGameEnd;

    public delegate void StartGameAction();
    public static event StartGameAction OnGameStart;

    public int currentScore { get; private set; }
    public int highScore { get; private set; }

    [SerializeField] private GameObject player;

    private enum GameState { FreshStart, Running, Ended };

    private GameState gameState;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            // Don't destroy on load if this is root object. If not root object, this does nothing.
            if (transform == null)
            {
                DontDestroyOnLoad(gameObject);
            }
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }


        gameState = GameState.FreshStart;
    }

    private void OnEnable()
    {
        PlayerCollideWithSomething.OnObstacleCollide += PlayerCollideWithObstacle_OnCollide;
        PlayerCollideWithSomething.OnPlayerScore += PlayerCollideWithSomething_OnScoreZoneCollide;
    }

    private void OnDisable()
    {
        PlayerCollideWithSomething.OnObstacleCollide -= PlayerCollideWithObstacle_OnCollide;
        PlayerCollideWithSomething.OnPlayerScore -= PlayerCollideWithSomething_OnScoreZoneCollide;
    }

    private void Update()
    {
        // Quit on Esc press.
        // On Android this is the back button.
        if (Input.GetKey(KeyCode.Escape))
        {
            Debug.Log("User triggered quit");
            Application.Quit();
        }


        if (CanStartGame())
        {
            if (gameState == GameState.FreshStart || gameState == GameState.Ended)
            {
                StartGame();
            }
        }
    }


    private void PlayerCollideWithSomething_OnScoreZoneCollide(int points)
    {
        PlayerScored(points);
    }

    private void PlayerCollideWithObstacle_OnCollide()
    {
        EndGame();
    }

    private void ResetScore()
    {
        currentScore = 0;
    }

    public void StartGame()
    {
        gameState = GameState.Running;

        ResetScore();

        if (OnGameStart != null)
        {
            OnGameStart();
        }
    }

    private void EndGame()
    {
        gameState = GameState.Ended;

        if (OnGameEnd != null)
        {
            OnGameEnd();
        }
    }

    private void PlayerScored(int points)
    {
        currentScore += points;

        if (currentScore > highScore)
        {
            highScore = currentScore;
        }
    }

    private bool CanStartGame()
    {
        if (gameState == GameState.Running)
        {
            return false;
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.R))
        {
            return true;
        }
#endif

        return (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began);
    }
}
