﻿using TMPro;
using UnityEngine;

public class ScoreUI : MonoBehaviour
{
    private TextMeshProUGUI text;

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
        ResetScoreText();
    }

    private void OnEnable()
    {
        GameController.OnGameStart += GameController_OnGameStart;
        GameController.OnGameEnd += GameController_OnGameEnd;
        PlayerCollideWithSomething.OnPlayerScore += PlayerCollideWithSomething_OnScoreZoneCollide;
    }

    private void OnDisable()
    {
        GameController.OnGameStart -= GameController_OnGameStart;
        GameController.OnGameEnd -= GameController_OnGameEnd;
        PlayerCollideWithSomething.OnPlayerScore -= PlayerCollideWithSomething_OnScoreZoneCollide;
    }

    private void GameController_OnGameEnd()
    {
        string scoreText = string.Format("{0}\nHigh score: {1}", GameController.instance.currentScore, GameController.instance.highScore);
        text.text = scoreText;
    }

    private void GameController_OnGameStart()
    {
        ResetScoreText();
    }

    private void PlayerCollideWithSomething_OnScoreZoneCollide(int points)
    {
        text.text = GameController.instance.currentScore.ToString();
    }

    private void ResetScoreText()
    {
        text.text = "0";
    }
}
