﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] List<Sound> sounds = new List<Sound>();

    Dictionary<string, Sound> soundsDict = new Dictionary<string, Sound>();

    private void OnEnable()
    {
        PlayerCollideWithSomething.OnPlayerScore += PlayerCollideWithSomething_OnPlayerScore;
        PlayerCollideWithSomething.OnObstacleCollide += PlayerCollideWithSomething_OnObstacleCollide;
        GameController.OnGameStart += GameController_OnGameStart;
    }

    

    private void OnDisable()
    {
        PlayerCollideWithSomething.OnPlayerScore -= PlayerCollideWithSomething_OnPlayerScore;
        PlayerCollideWithSomething.OnObstacleCollide -= PlayerCollideWithSomething_OnObstacleCollide;
        GameController.OnGameStart -= GameController_OnGameStart;

    }

    private void PlayerCollideWithSomething_OnPlayerScore(int points)
    {
        PlaySound(Names.ScoreSound);
    }

    private void PlayerCollideWithSomething_OnObstacleCollide()
    {
        PlaySound(Names.CollisionSound);
    }

    private void GameController_OnGameStart()
    {
        PlaySound(Names.EngineSound);
    }

    private void Awake()
    {
        foreach(Sound sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.InitSource();
            soundsDict.Add(sound.name, sound);
        }
    }

    public void PlaySound(string soundName)
    {
        Sound sound;

        if(soundsDict.TryGetValue(soundName, out sound))
        {
            sound.source.Stop();
            sound.source.Play();
        }
        else
        {
            Debug.LogWarning(string.Format("Tried to play sound {0} but it's not defined", soundName));
        }
    }




}
