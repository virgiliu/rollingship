﻿using UnityEngine;

public class ConstantMove : MonoBehaviour
{
    [HideInInspector] public Vector3 velocity = Vector3.zero;

    private void OnEnable()
    {
        GameController.OnGameEnd += GameController_OnGameEnd;
    }

    private void OnDisable()
    {
        GameController.OnGameEnd -= GameController_OnGameEnd;
    }

    private void GameController_OnGameEnd()
    {
        velocity = Vector3.zero;
    }

    private void Update()
    {
        transform.Translate(velocity * Time.deltaTime);
    }
}
