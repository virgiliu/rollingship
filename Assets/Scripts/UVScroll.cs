﻿using UnityEngine;

public class UVScroll : MonoBehaviour
{
    public int materialIndex = 0;
    public Vector2 uvAnimationRate = new Vector2(1.0f, 0.0f);
    public string textureName = "_MainTex";
    private Vector2 uvOffset = Vector2.zero;
    private Renderer rend;

    private bool shouldMove = false;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    private void OnEnable()
    {
        GameController.OnGameStart += GameController_OnGameStart;
        GameController.OnGameEnd += GameController_OnGameEnd;
    }

    private void OnDisable()
    {
        GameController.OnGameStart -= GameController_OnGameStart;
        GameController.OnGameEnd -= GameController_OnGameEnd;
    }

    private void GameController_OnGameStart()
    {
        shouldMove = true;
    }

    private void GameController_OnGameEnd()
    {
        shouldMove = false;
    }

    private void LateUpdate()
    {
        if (shouldMove)
        {
            uvOffset += (uvAnimationRate * Time.deltaTime);
            if (rend.enabled)
            {
                rend.materials[materialIndex].SetTextureOffset(textureName, uvOffset);
            }
        }
    }
}
