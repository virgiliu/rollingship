﻿using UnityEngine;

public class SpawnPickup : MonoBehaviour
{
    [SerializeField] private Transform spawnTransform;
    [SerializeField] private GameObject pickupPrefab;

    private void Awake()
    {
        // Spawn only 3% of the time
        if (Random.Range(0.00f, 1.00f) <= .15f)
        {
            GameObject pickup = Instantiate(pickupPrefab, spawnTransform);
            pickup.transform.localPosition = Vector3.zero;

        }
    }


}
