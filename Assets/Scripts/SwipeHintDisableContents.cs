﻿using UnityEngine;

public class SwipeHintDisableContents : MonoBehaviour
{
    private void OnEnable()
    {
        GameController.OnGameStart += GameController_OnGameStart;
        GameController.OnGameEnd += GameController_OnGameEnd;
    }

    private void OnDisable()
    {
        GameController.OnGameStart -= GameController_OnGameStart;
        GameController.OnGameEnd -= GameController_OnGameEnd;
    }

    private void GameController_OnGameStart()
    {
        DisableChildren();
    }

    private void GameController_OnGameEnd()
    {
        EnableChildren();
    }

    private void DisableChildren()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
    }

    private void EnableChildren()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }
    }
}
