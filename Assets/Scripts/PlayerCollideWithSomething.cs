﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class PlayerCollideWithSomething : MonoBehaviour
{
    public delegate void ObstacleCollideAction();
    public static event ObstacleCollideAction OnObstacleCollide;

    public delegate void PlayerScoreAction(int points);
    public static event PlayerScoreAction OnPlayerScore;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == Names.ObstacleTag)
        {
            if(OnObstacleCollide != null)
            {
                OnObstacleCollide();
            }
        }

        else if(other.tag == Names.ScoreZoneTag)
        {
            if(OnPlayerScore != null)
            {
                OnPlayerScore(1);
            }
        }
        else if (other.tag == Names.PickupTag)
        {
            if (OnPlayerScore != null)
            {
                OnPlayerScore(3);
            }

            Destroy(other.gameObject);
        }
    }
}
